function tinhGocLech() {
  let soGio = document.getElementById(`sogio`).value * 1;
  let soPhut = document.getElementById(`sophut`).value * 1;

  let soDoPhut = soPhut * (360 / 60);
  let doLechGio = 30 / 60;
  let soDoGio = doLechGio * (soGio * 60 + soPhut);

  let result = Math.abs(soDoPhut - soDoGio);

  document.getElementById(
    `result-ex10`
  ).innerHTML = `Thời gian: ${soGio} giờ ${soPhut} phút. <br> Kim giờ và kim phút lệch nhau ${result} độ`;

  $("#form-10").submit(function (e) {
    e.preventDefault();
  });
}
