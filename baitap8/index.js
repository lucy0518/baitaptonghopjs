function chiaBai() {
  let players = [[], [], [], []];

  var player1 = players[0];
  var player2 = players[1];
  var player3 = players[2];
  var player4 = players[3];

  let cards = [
    "4K",
    "KH",
    "5C",
    "KA",
    "QH",
    "KD",
    "2H",
    "10S",
    "AS",
    "7H",
    "9K",
    "10D",
  ];

  for (var i = 1; i <= cards.length; i++) {
    var chosenCard = cards[i];
    var cardForPlayer1 = cards.splice(chosenCard, 1);
    player1.push(cardForPlayer1);

    var cardForPlayer2 = cards.splice(chosenCard, 1);
    player2.push(cardForPlayer2);

    var cardForPlayer3 = cards.splice(chosenCard, 1);
    player3.push(cardForPlayer3);

    var cardForPlayer4 = cards.splice(chosenCard, 1);
    player4.push(cardForPlayer4);
  }

  document.getElementById(
    `result-ex8`
  ).innerHTML = `Kết quả chia bài là:<br> Player 1= ${player1} <br> Player 2= ${player2} <br> Player 3= ${player3} <br> Player 4= ${player4}`;

  $("#form-8").submit(function (e) {
    e.preventDefault();
  });
}
